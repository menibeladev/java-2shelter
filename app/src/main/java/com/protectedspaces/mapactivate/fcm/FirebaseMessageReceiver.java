package com.protectedspaces.mapactivate.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.protectedspaces.mapactivate.MapsActivity;
import com.protectedspaces.mapactivate.R;
import com.protectedspaces.mapactivate.RecentAlarms;
//import com.protectedspaces.mapactivate.connect;

public class FirebaseMessageReceiver extends FirebaseMessagingService {
    String TAG = "Message";

    @Override
    public void onMessageReceived (RemoteMessage remoteMessage) {
        if (remoteMessage.getData ().size () > 0) {
            Log.d (TAG, "Message data payload: " + remoteMessage.getData ());
            sendNotification (remoteMessage.getNotification ().getBody (), remoteMessage.getNotification ().getTitle ());
        }
        if (remoteMessage.getNotification () != null) {
            sendNotification (remoteMessage.getNotification ().getBody (), remoteMessage.getNotification ().getTitle ());
            Log.d (TAG, "Message Notification Body: " + remoteMessage.getNotification ().getBody ());
        }
        Intent intent = new Intent ("msg");    //action: "msg"
        intent.setPackage (getPackageName ());
        //intent.putExtra("message", message.getBody());
        getApplicationContext ().sendBroadcast (intent);
    }

    @Override
    public void onNewToken (String token) {
        Log.d (TAG, "Refreshed token: " + token);
    }

    private void sendNotification (String messageBody, String title) {
        Intent intent = new Intent (this, MapsActivity.class);
        intent.addFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity (this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString (R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri (RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder (this, channelId)
                        .setSmallIcon (R.drawable.ic_launcher_background)
                        .setContentText (messageBody)
                        .setAutoCancel (true)
                        .setContentTitle (title)
                        .setSound (defaultSoundUri)
                        .setSmallIcon (R.drawable.ic_notification)
                        .setContentIntent (pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService (Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel (channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel (channel);

        }
        notificationManager.notify (0 /* ID of notification */, notificationBuilder.build ());
    }
}
