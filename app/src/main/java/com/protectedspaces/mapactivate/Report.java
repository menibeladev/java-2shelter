package com.protectedspaces.mapactivate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class Report extends AppCompatActivity {

    EditText editTextTo, editTextSubject, editTextMessage;
    Button buttonSend;
    String to, subject, message;
    Intent intentMail;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_report);

        editTextTo = (EditText) findViewById (R.id.editTo);
        editTextSubject = (EditText) findViewById (R.id.editSubject);
        editTextMessage = (EditText) findViewById (R.id.editMessage);
        buttonSend = (Button) findViewById (R.id.btnSend);

        buttonSend.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                GetData ();

                intentMail = new Intent (Intent.ACTION_SEND);
                intentMail.putExtra (Intent.EXTRA_EMAIL, new String[]{to});
                intentMail.putExtra (Intent.EXTRA_SUBJECT, subject);
                intentMail.putExtra (Intent.EXTRA_TEXT, message);

                intentMail.setType ("message/rfc822");
                startActivity (Intent.createChooser (intentMail, "Select Email Sending Apps"));

            }
        });
    }

    private void GetData () {
        to = editTextTo.getText ().toString ();
        subject = editTextSubject.getText ().toString ();
        message = editTextMessage.getText ().toString ();
    }

}

