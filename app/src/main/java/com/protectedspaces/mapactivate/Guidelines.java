package com.protectedspaces.mapactivate;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Guidelines extends AppCompatActivity {

    Dialog epicDialog;
    Button btnAccept;
    ImageView closePopup, homePopupBtn, carPopupBtn, transportPopupBtn, spacePopupBtn;

    @Override
    protected void onCreate (Bundle savedInstanceState) {

        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_guidelines);
        setTitle ("הנחיות בזמן ההאזעקה");

        epicDialog = new Dialog (this);
        homePopupBtn = (ImageView) findViewById (R.id.homePopupBtn);
        carPopupBtn = (ImageView) findViewById (R.id.carPopupBtn);
        transportPopupBtn = (ImageView) findViewById (R.id.transportPopupBtn);
        spacePopupBtn = (ImageView) findViewById (R.id.openSpacePopupBtn);

        homePopupBtn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                openDialogHome ();
            }
        });

        carPopupBtn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                openDialogCar ();
            }
        });

        transportPopupBtn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                openDialogTransport ();
            }
        });

        spacePopupBtn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                openDialogOpenSpace ();
            }
        });
    }

    public void openDialogHome () {
        epicDialog.setContentView (R.layout.home_popup);
        allPopup ();
    }

    public void openDialogCar () {
        epicDialog.setContentView (R.layout.car_popup);
        allPopup ();
    }

    public void openDialogTransport () {
        epicDialog.setContentView (R.layout.transport_popup);
        allPopup ();
    }

    public void openDialogOpenSpace () {
        epicDialog.setContentView (R.layout.open_space_popup);
        allPopup ();
    }

    private void allPopup () {
        closePopup = (ImageView) epicDialog.findViewById (R.id.closePopup);
        btnAccept = (Button) epicDialog.findViewById (R.id.btnAccept);
        closePopup.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                epicDialog.dismiss ();
            }
        });

        btnAccept.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                epicDialog.dismiss ();
            }
        });

        epicDialog.getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
        epicDialog.show ();
    }
}
