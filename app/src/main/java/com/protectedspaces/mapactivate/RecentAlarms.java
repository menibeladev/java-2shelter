package com.protectedspaces.mapactivate;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecentAlarms extends AppCompatActivity {

    private static final String TAG ="RecentAlarms";
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_alarms);
        setTitle("אזעקות אחרונות");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }



    public List<LocationDTO> getLocations(View view) throws IOException {
        /*
        The solution of this exception:
         Method threw 'andriod.os.NetworkOnMainThreadException' exception  */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Call<List<LocationDTO>> call = apiInterface.getAllLocations();
        List<LocationDTO> locationsList = call.execute().body();
        int ten = 10;
        return locationsList;
    }

}
