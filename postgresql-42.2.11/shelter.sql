create schema protected_spaces;

CREATE TABLE protected_spaces.locations
(
    serial_number integer               NOT NULL,
    address       character varying(50) NOT NULL,
    x             numeric               NOT NULL,
    y             numeric               NOT NULL,
    max_contents  integer               NOT NULL
);

INSERT INTO protected_spaces.locations(serial_number, address, x, y, max_contents)
VALUES (50, 'goyava', 34.584211, 31.416259, 30),
       (49, 'hadas', 34.584274, 31.415499, 30),
       (48, 'zayet', 34.582887, 31.415536, 30),
       (47, 'etrog', 34.582214, 31.414433, 30),
       (46, 'arava', 34.580816, 31.413619, 30),
       (45, 'tamar', 34.579499, 31.413376, 30),
       (44, 'escholite', 34.580461, 31.411756, 30),
       (43, 'gefen', 34.580528, 31.41227, 30),
       (42, 'gefen', 34.581961, 31.413094, 30),
       (41, 'gefen', 34.5835, 31.413672, 30),
       (40, 'tot', 34.585398, 31.414917, 30),
       (100, 'sarigA', 34.806039, 31.283474, 30),
       (101, 'sarigB', 34.805622, 31.281061, 30),
       (102, 'sarigC', 34.804718, 31.280857, 30),
       (103, 'MeniA', 34.5809078, 31.5309800, 30),
       (104, 'MeniB', 34.5811834, 31.5311011, 30),
       (105, 'MeniC', 34.5812058, 31.5310743, 30),
       (106, 'ShirA', 34.5218, 31.6100, 30),
       (108, 'ShirC', 34.5232, 31.6099, 30),
       (107, 'ShirB', 34.5213614, 31.6099128, 30)
;
