package com.protectedspaces.api;


import com.protectedspaces.commons.model.vo.LocationsVO;
import com.protectedspaces.commons.model.dto.LocationsDTO;
import com.protectedspaces.logic.LocationMapper;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

@RestController
public class RestApi {

    @Resource
    private LocationMapper LogicLayer;

    @RequestMapping("/")
    public List<LocationsVO> getLocation() throws SQLException {
        List<LocationsVO> locationsVO = LogicLayer.getAllLocationsVo();
        return locationsVO;
    }



}
