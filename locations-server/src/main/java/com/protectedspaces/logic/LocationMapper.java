package com.protectedspaces.logic;

import com.protectedspaces.dal.DbConnection;
import com.protectedspaces.commons.model.dto.LocationsDTO;
import com.protectedspaces.commons.model.vo.LocationsVO;
import com.protectedspaces.dal.GetLocations;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class LocationMapper {


    //@android.support.annotation.RequiresApi(api = android.os.Build.VERSION_CODES.N)
    public List<LocationsDTO> getAllLocations() throws SQLException {
        GetLocations getLocations = new GetLocations();
        List<LocationsVO> locations = getLocations.getAllLocations();
        return locations
                .stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    public List<LocationsVO> getAllLocationsVo() throws SQLException {
        GetLocations getLocations = new GetLocations();
        List<LocationsVO> locations = getLocations.getAllLocations();
        return locations;
    }


    public LocationsDTO mapToDTO(LocationsVO locationsVO){
        LocationsDTO locationsDTO = new LocationsDTO();

        locationsDTO.setAddress(locationsVO.getAddress());
        locationsDTO.setMaxContents(locationsVO.getMaxContents());
        locationsDTO.setSerialNumber(locationsVO.getSerialNumber());
        locationsDTO.setX(locationsVO.getX());
        locationsDTO.setY(locationsVO.getY());

        return locationsDTO;
    }

    public LocationsVO mapToVO(LocationsDTO locationsDTO){
        LocationsVO locationsVO = new LocationsVO();

        locationsVO.setAddress(locationsDTO.getAddress());
        locationsVO.setMaxContents(locationsDTO.getMaxContents());
        locationsVO.setSerialNumber(locationsDTO.getSerialNumber());
        locationsVO.setX(locationsDTO.getX());
        locationsVO.setY(locationsDTO.getY());

        return locationsVO;
    }
}
