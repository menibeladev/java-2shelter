package com.protectedspaces.commons.model.vo;

public class LocationsVO {

    private int serialNumber;
    private String address;
    private double x;
    private double y;
    private int maxContents;


    public LocationsVO(){}

    public int getSerialNumber() {return this.serialNumber; }
    public String getAddress(){return this.address; }
    public double getX(){return this.x; }
    public double getY(){return this.y; }
    public int getMaxContents() {return this.maxContents; }

    public void setSerialNumber(int serialNumber) {this.serialNumber=serialNumber; }
    public void setAddress(String address){this.address=address; }
    public void setX(double x){this.x=x; }
    public void setY(double y){this.y=y; }
    public void setMaxContents(int maxContents) {this.maxContents=maxContents; }
}
