package com.protectedspaces.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class DbConnection {

    private  String database_connection_string = "jdbc:postgresql://localhost:5432/shell";
    private  String database_user_name = "postgres";
    private  String database_user_password = "31933133";

    public Connection getConnection() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(database_connection_string, database_user_name, database_user_password );
            System.out.println("You are successfully connected to the PostgreSQL database server.");
        } catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        return conn;
    }





}