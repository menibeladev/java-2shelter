package com.protectedspaces.dal;

import com.protectedspaces.commons.model.vo.LocationsVO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetLocations {

   // private Statement statement;
    private PreparedStatement preparedStatement;

    public List<LocationsVO> getAllLocations() throws SQLException {

        DbConnection connection = new DbConnection();
        Connection conn = connection.getConnection();
        preparedStatement = conn.prepareStatement("select * from locations");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<LocationsVO> locationsVOs = new ArrayList<>();

        while (resultSet.next()) {
            LocationsVO location = new LocationsVO();
            location.setAddress(resultSet.getString("address"));
            location.setMaxContents(resultSet.getInt("max_contents"));
            location.setSerialNumber(resultSet.getInt("serial_number"));
            location.setX(resultSet.getDouble("x"));
            location.setY(resultSet.getDouble("y"));

            locationsVOs.add(location);
        }
        return locationsVOs;
    }


    public static void main(String[] args) throws SQLException {

        GetLocations test = new GetLocations();
        List<LocationsVO> tt =  test.getAllLocations();

       int x = 3;
    }
}
